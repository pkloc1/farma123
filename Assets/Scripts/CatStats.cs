﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using static System.Collections.Specialized.BitVector32;

public class CatStats : MonoBehaviour
{
    public float WeedTheBedsNeed { get; private set; } = 50f;
    public float WaterTheBedsNeed { get; private set; } = 50f;
    public float PickTheFruitsNeed { get; private set; } = 65f;
    public float MilkingAСowNeed { get; private set; } = 48f;
    public float WorkWithForgeNeed { get; private set; } = 92f;
    public float TrainWithSwordNeed { get; private set; } = 86f;
    public float ShoppingNeed { get; private set; } = 40f;

    public Animator animator;
    public bool isDance;
    private NavMeshAgent agent;

    [SerializeField] private Transform weedTheBedsTransform;
    [SerializeField] private Transform waterTheBedsNedTransform;
    [SerializeField] private Transform wickTheFruitsNeedTransform;
    [SerializeField] private Transform milkingAСowNeedTransform;
    [SerializeField] private Transform workWithForgeNeedTransform;
    [SerializeField] private Transform trainWithSwordNeedTransform;
    [SerializeField] private Transform shoppingNeedTransform;
    [SerializeField] private Transform danceTransform;

    [SerializeField] GameObject wateringCan;
    [SerializeField] GameObject fruitBucket;
    [SerializeField] GameObject sword;
    [SerializeField] GameObject hammer;

    private float maxStat = 100f;
    private bool isBusy = false;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (!isBusy)
        {
            IncreaseStatOverTime();
            CheckStats();
        }
        InstrumentsHandle();
    }

    private void IncreaseStatOverTime()
    {
        WeedTheBedsNeed += Time.deltaTime / 1.35f;
        WaterTheBedsNeed += Time.deltaTime / 1.2f;
        PickTheFruitsNeed += Time.deltaTime / 1.1f;
        MilkingAСowNeed += Time.deltaTime / 1.3f;
        WorkWithForgeNeed += Time.deltaTime / 1f;
        TrainWithSwordNeed += Time.deltaTime / 0.9f;
        ShoppingNeed += Time.deltaTime / 1.5f;

        Debug.Log(WeedTheBedsNeed + "Weet The Beds");
        Debug.Log(WaterTheBedsNeed + "Water The Beds");
        Debug.Log(PickTheFruitsNeed + "Fruits");
        Debug.Log(MilkingAСowNeed + "Milk");
        Debug.Log(WorkWithForgeNeed + "ForgeWork");
        Debug.Log(TrainWithSwordNeed + "Train");
        Debug.Log(ShoppingNeed + "Shop");

        WeedTheBedsNeed = Mathf.Clamp(WeedTheBedsNeed, 0, maxStat);
        WaterTheBedsNeed = Mathf.Clamp(WaterTheBedsNeed, 0, maxStat);
        PickTheFruitsNeed = Mathf.Clamp(PickTheFruitsNeed, 0, maxStat);
        MilkingAСowNeed = Mathf.Clamp(MilkingAСowNeed, 0, maxStat);
        WorkWithForgeNeed = Mathf.Clamp(WorkWithForgeNeed, 0, maxStat);
        TrainWithSwordNeed = Mathf.Clamp(TrainWithSwordNeed, 0, maxStat);
        ShoppingNeed = Mathf.Clamp(ShoppingNeed, 0, maxStat);
    }

    private void CheckStats()
    {
        if (WeedTheBedsNeed >= maxStat)
        {
            MoveToReplenish("WeedTheBeds");
        }

        if (WaterTheBedsNeed >= maxStat)
        {
            MoveToReplenish("WaterTheBeds");
        }

        if (PickTheFruitsNeed >= maxStat)
        {
            MoveToReplenish("PickTheFruits");
        }

        if (MilkingAСowNeed >= maxStat)
        {
            MoveToReplenish("MilkingAСow");
        }

        if (WorkWithForgeNeed >= maxStat)
        {
            MoveToReplenish("WorkWithForge");
        }

        if (TrainWithSwordNeed >= maxStat)
        {
            MoveToReplenish("TrainWithSword");
        }

        if (ShoppingNeed >= maxStat)
        {
            MoveToReplenish("Shopping");
        }
        else if (!AnyStatAtMax())
        {
            MoveToDance();
            isDance = true;
        }

    }

    private void MoveToReplenish(string statType)
    {
        isDance = false;
        animator.SetBool("Dance", false);
        GameObject replenishStation = GameObject.FindGameObjectWithTag(statType);
        Debug.Log(statType);
        if (replenishStation != null)
        {
            agent.SetDestination(replenishStation.transform.position);
            StartCoroutine(ReplenishAtStation(statType, replenishStation));
        }
    }

    private IEnumerator ReplenishAtStation(string statType, GameObject station)
    {
        isBusy = true;

        while (Vector3.Distance(transform.position, station.transform.position) > 1)
        {
            Debug.Log("moving to " + statType);
            yield return null;
        }
        animator.SetBool(statType, true);

        AudioSource audioSource = station.GetComponent<AudioSource>();
        if (audioSource != null)
        {
            audioSource.Play();
        }

        yield return new WaitForSeconds(20);
        Debug.Log("GOOOOOOO");
        audioSource.Stop();

        animator.SetBool(statType, false);

        switch (statType)
        {
            case "WeedTheBeds":
                WeedTheBedsNeed = 0;
                break;
            case "WaterTheBeds":
                WaterTheBedsNeed = 0;
                break;
            case "PickTheFruits":
                PickTheFruitsNeed = 0;
                break;
            case "MilkingAСow":
                MilkingAСowNeed = 0;
                break;
            case "WorkWithForge":
                WorkWithForgeNeed = 0;
                break;
            case "TrainWithSword":
                TrainWithSwordNeed = 0;
                break;
            case "Shopping":
                ShoppingNeed = 0;
                break;
        }

        isBusy = false;
    }

    private void MoveToDance()
    {
        agent.SetDestination(danceTransform.position);
        if (Vector3.Distance(transform.position, danceTransform.position) < 1)
        {
            animator.SetBool("Dance", true);
        }
    }

    private bool AnyStatAtMax()
    {
        return WeedTheBedsNeed >= maxStat 
            || WaterTheBedsNeed >= maxStat 
            || PickTheFruitsNeed >= maxStat 
            || MilkingAСowNeed >= maxStat 
            || WorkWithForgeNeed >= maxStat 
            || TrainWithSwordNeed >= maxStat 
            || ShoppingNeed >= maxStat; 
    }

    private void InstrumentsHandle()
    {
        if (WaterTheBedsNeed >= maxStat)
        {
            wateringCan.SetActive(true);
        }
        else
        {
            wateringCan.SetActive(false);
        }
        if (PickTheFruitsNeed >= maxStat)
        {
            fruitBucket.SetActive(true);
        }
        else
        {
            fruitBucket.SetActive(false);
        }
        if (TrainWithSwordNeed >= maxStat)
        {
            sword.SetActive(true);
        }
        else
        {
            sword.SetActive(false);
        }
        if (WorkWithForgeNeed >= maxStat)
        {
            hammer.SetActive(true);
        }
        else
        {
            hammer.SetActive(false);
        }
    }
}
