﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatUI : MonoBehaviour
{
    public CatStats catStats;
    public Slider weedTheBedsSlider;
    public Slider waterTheBedsSlider;
    public Slider pickTheFruitsSlider;
    public Slider milkingAСowSlider;
    public Slider workWithForgeSlider;
    public Slider trainWithSwordSlider;
    public Slider shoppingSlider;

    public Image plantImage;
    public Image wateringImage;
    public Image fruitImage;
    public Image milkImage;
    public Image forgeImage;
    public Image trainImage;
    public Image shopImage;
    public Image danceImage;


    void Update()
    {
        weedTheBedsSlider.value = catStats.WeedTheBedsNeed / 100f;
        if (catStats.WeedTheBedsNeed >= 100)
        {
            plantImage.gameObject.SetActive(true);
        }
        else
        {
            plantImage.gameObject.SetActive(false);
        }
        waterTheBedsSlider.value = catStats.WaterTheBedsNeed / 100f;
        if (catStats.WaterTheBedsNeed >= 100)
        {
            wateringImage.gameObject.SetActive(true);
        }
        else
        {
            wateringImage.gameObject.SetActive(false);
        }
        pickTheFruitsSlider.value = catStats.PickTheFruitsNeed / 100f;
        if (catStats.PickTheFruitsNeed >= 100)
        {
            fruitImage.gameObject.SetActive(true);
        }
        else
        {
            fruitImage.gameObject.SetActive(false);
        }
        milkingAСowSlider.value = catStats.MilkingAСowNeed / 100f;
        if (catStats.MilkingAСowNeed >= 100)
        {
            milkImage.gameObject.SetActive(true);
        }
        else
        {
            milkImage.gameObject.SetActive(false);
        }
        workWithForgeSlider.value = catStats.WorkWithForgeNeed / 100f;
        if (catStats.WorkWithForgeNeed >= 100)
        {
            forgeImage.gameObject.SetActive(true);
        }
        else
        {
            forgeImage.gameObject.SetActive(false);
        }
        trainWithSwordSlider.value = catStats.TrainWithSwordNeed / 100f;
        if (catStats.TrainWithSwordNeed >= 100)
        {
            trainImage.gameObject.SetActive(true);
        }
        else
        {
            trainImage.gameObject.SetActive(false);
        }
        shoppingSlider.value = catStats.ShoppingNeed / 100f;
        if (catStats.ShoppingNeed >= 100)
        {
            shopImage.gameObject.SetActive(true);
        }
        else
        {
            shopImage.gameObject.SetActive(false);
        }

        if (catStats.isDance == true)
        {
            danceImage.gameObject.SetActive(true);
        }
        else
        {
            danceImage.gameObject.SetActive(false);
        }
    }
}
